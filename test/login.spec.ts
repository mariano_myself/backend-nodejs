import 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
let should = chai.should();

let url = 'http://localhost:3000';

describe('Login function', () => {
  describe('Success Request', () => {
    it('Should return token', (done) => {
      chai.request(url)
      .post('/login')
      .type('form')
      .send({
        'email': 'britneyblankenship@quotezart.com',
        'password': '123456'
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('token');
        done();
      });
    });
  });

  describe('Unhautorized 1', () => {
    it('Should return unhautorized email', (done) => {
      chai.request(url)
      .post('/login')
      .type('form')
      .send({
        'email': 'mail@quotezart.com',
        'password': '123456'
      })
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.deep.own.property('error', {message: 'Email is not valid', status: 401});
        done();
      });
    });
  });

  describe('Unhautorized 2', () => {
    it('Should return unhautorized password', (done) => {
      chai.request(url)
      .post('/login')
      .type('form')
      .send({
        'email': 'britneyblankenship@quotezart.com',
        'password': '1234567'
      })
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.deep.own.property('error', {message: 'Password is not valid', status: 401});
        done();
      });
    });
  });
});
