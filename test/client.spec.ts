import 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
let should = chai.should();

let url = 'http://localhost:3000';
let token;

describe('Client REST API Testing', () => {

  before(function (done) {
    this.enableTimeouts(false)
    chai.request(url)
    .post('/login')
    .type('form')
    .send({
      'email': 'britneyblankenship@quotezart.com',
      'password': '123456'
    })
    .end((err, res) => {
      res.should.have.status(200);
      token = res.body.token;
      done();
    })
  });

  describe('Get user data filtered by user id', () => {
    it('Should return Client Britney', (done) => {
      chai.request(url)
      .get('/client/a0ece5db-cd14-4f21-812f-966633e7be86')
      .set('token', token)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(true);
        res.body.should.have.property('client');
        done();
      });
    }).timeout(5000);

    it('Should return Unhautorized', (done) => {
      chai.request(url)
      .get('/client/a0ece5db-cd14-4f21-812f-966633e7be86')
      .set('token', 'fakeToken')
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });

    it('Should return Bad Request', (done) => {
      chai.request(url)
      .get('/client')
      .set('token', token)
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });

    it('Should return Not Found', (done) => {
      chai.request(url)
      .get('/client/123')
      .set('token', token)
      .end((err, res) => {
        res.should.have.status(404);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });
  });

  describe('Get user data filtered by user name', () => {
    it('Should return Client Britney', (done) => {
      chai.request(url)
      .get('/client')
      .set('token', token)
      .query({name: 'Britney'})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(true);
        res.body.should.have.property('client');
        done();
      });
    }).timeout(5000);

    it('Should return Unhautorized', (done) => {
      chai.request(url)
      .get('/client/a0ece5db-cd14-4f21-812f-966633e7be86')
      .set('token', 'fakeToken')
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });

    it('Should return Bad Request', (done) => {
      chai.request(url)
      .get('/client')
      .set('token', token)
      .query({fake: 'Britney'})
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });

    it('Should return Not Found', (done) => {
      chai.request(url)
      .get('/client/123')
      .set('token', token)
      .query({name: 'fakeName'})
      .end((err, res) => {
        res.should.have.status(404);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });
  });

  describe('Get the user linked to a policy number', () => {
    it('Should return Client Britney', (done) => {
      chai.request(url)
      .get('/client')
      .set('token', token)
      .query({'policyName': '7b624ed3-00d5-4c1b-9ab8-c265067ef58b'})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(true);
        res.body.should.have.property('client');
        done();
      });
    }).timeout(5000);
  });
});
