import 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

chai.use(chaiHttp);
let should = chai.should();

let url = 'http://localhost:3000';
let token;
let tokenUserRole;

describe('Client REST API Testing', () => {

  before(function (done) {
    this.enableTimeouts(false)
    chai.request(url)
    .post('/login')
    .type('form')
    .send({
      'email': 'britneyblankenship@quotezart.com',
      'password': '123456'
    })
    .end((err, res) => {
      res.should.have.status(200);
      token = res.body.token;
      done();
    })
  });

  before(function (done) {
    this.enableTimeouts(false)
    chai.request(url)
    .post('/login')
    .type('form')
    .send({
      'email': 'baxterblankenship@quotezart.com',
      'password': '123456'
    })
    .end((err, res) => {
      res.should.have.status(200);
      tokenUserRole = res.body.token;
      done();
    })
  });

  describe('Get the list of policies linked to a user name', () => {
    it('Should return a list of policies', (done) => {
      chai.request(url)
      .get('/policy')
      .set('token', token)
      .query({'userName': 'Britney'})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(true);
        res.body.should.have.property('policies');
        res.body.policies.should.to.be.an.instanceof(Array);
        done();
      });
    }).timeout(5000);

    it('Should return Unhautorized for Token', (done) => {
      chai.request(url)
      .get('/policy')
      .set('token', 'fakeToken')
      .query({'userName': 'Britney'})
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        res.body.should.deep.own.property('error', {message: 'Token is not valid', status: 401});
        done();
      });
    });

    it('Should return Unhautorized for Role', (done) => {
      chai.request(url)
      .get('/policy')
      .set('token', tokenUserRole)
      .query({'userName': 'Britney'})
      .end((err, res) => {
        res.should.have.status(401);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        res.body.should.deep.own.property('error', {message: 'Role is not valid', status: 401});
        done();
      });
    });

    it('Should return Bad Request', (done) => {
      chai.request(url)
      .get('/policy')
      .set('token', token)
      .query({'fakeQuery': 'Britney'})
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    });

    it('Should return Not Found', (done) => {
      chai.request(url)
      .get('/policy')
      .set('token', token)
      .query({'userName': 'FakeUserName'})
      .end((err, res) => {
        res.should.have.status(404);
        res.body.should.be.a('object');
        res.body.should.have.property('ok').eq(false);
        done();
      });
    }).timeout(5000);;
  });
});
