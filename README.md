# BackEnd-NodeJS

## Run your application

Configure a MongoDB instance

Download the code from: https://msanchez90@bitbucket.org/mariano_myself/backend-nodejs.git

At the console put: npm install

You have to configure your own configuration properties in ".env" file at the same level of src folder.
An example:

	PORT = '3000'
	CADUCIDAD_TOKEN = '30d'
	SEED = 'este-es-el-seed-desarrollo'
	URLDB = 'mongodb://localhost:27017/insuranceCompany'

Then you can run it with: npm run watch


## Testing

For testing we use Mocha and Chai. You have to have runnning your server and database, and registered these two Users:

	email: britneyblankenship@quotezart.com
	password: 123456
	
	email: baxterblankenship@quotezart.com
	password: 123456

Then you can run the tests with: npm test
