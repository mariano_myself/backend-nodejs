import * as mongoose from 'mongoose';

import IClient from './interfaces/client';

const Schema = mongoose.Schema;

export const ClientSchema = new Schema({
  id: {
    type: String
  },
  name: {
    type: String
  },
  email: {
    type: String
  },
  role: {
    type: String,
    enum: ['admin', 'user']
  }
});

const Client = mongoose.model<IClient & mongoose.Document>('Client', ClientSchema);
export default Client;
