import * as mongoose from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';

import IUser from './interfaces/user';

const Schema = mongoose.Schema;

export const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: [true, 'E-mail is Required']
  },
  password: {
    type: String,
    required: [true, 'Password is Required']
  }
});

UserSchema.plugin(uniqueValidator, {
  message: '{PATH} debe de ser unico'
})

UserSchema.methods.toJSON = function() {
  let user = this;
  let userObject = user.toObject();
  delete userObject.password;

  return userObject;
}

const User = mongoose.model<IUser & mongoose.Document>('User', UserSchema);
export default User;
