import * as mongoose from 'mongoose';

import IPolicy from './interfaces/policy';

const Schema = mongoose.Schema;

export const PolicySchema = new Schema({
  id: {
    type: String
  },
  amountInsured: {
    type: Number
  },
  email: {
    type: String
  },
  inceptionDate: {
    type: String
  },
  installmentPayment: {
    type: Boolean
  },
  clientId: {
    type: String
  }
});

const Policy = mongoose.model<IPolicy & mongoose.Document>('Client', PolicySchema);
export default Policy;
