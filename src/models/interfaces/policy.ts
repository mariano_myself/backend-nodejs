export default interface IPolicy {
  id: string;
  amountInsured: number;
  email: string;
  inceptionDate: string;
  installmentPayment: boolean;
  clientId: string;
}
