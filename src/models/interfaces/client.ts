export default interface IClient {
  id: string;
  name: string;
  email: string;
  role: string;
}
