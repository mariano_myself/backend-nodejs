import { Request } from 'express';
import IUser from './user';

export default interface IRequestWithUser extends Request {
  user: IUser;
}
