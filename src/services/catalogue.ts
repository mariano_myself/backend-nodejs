import axios from "axios";
import IClient from "../models/interfaces/client";
import IPolicy from "../models/interfaces/policy";

export default class CatalogueService {

  private async getAllClients(): Promise<IClient[]> {
    let clients = await axios.get(process.env.URLClients);
    return clients.data.clients;
  }

  public async getClientByID(id: String): Promise<IClient> {
    let clients = await this.getAllClients();
    let clientFounded = clients.filter(client => client.id === id)[0];
    return clientFounded;
  }

  public async getClientByName(name: String): Promise<IClient> {
    let clients = await this.getAllClients();
    let clientFounded = clients.filter(client => client.name === name)[0];
    return clientFounded;
  }

  public async getClientByEmail(email: String): Promise<IClient> {
    let clients = await this.getAllClients();
    let clientFounded = clients.filter(client => client.email === email)[0];
    return clientFounded;
  }

  public async getClientByPolicyId(policyId: String): Promise<any> {
    let policy = await this.getPolociesById(policyId);
    let clientFounded = await this.getClientByID(policy.clientId);
    return clientFounded;
  }

  private async getAllPolicies(): Promise<IPolicy[]> {
    let policies = await axios.get(process.env.URLPolicies);
    return policies.data.policies;
  }

  public async getPolociesById(id: String): Promise<IPolicy> {
    let policies = await this.getAllPolicies();
    let policyFounded = policies.filter(policie => policie.id === id)[0];
    return policyFounded;
  }

  public async getPolociesByUserName(userName: String): Promise<IPolicy[]> {
    let policiesFounded = null;
    let policies = await this.getAllPolicies();
    let user = await this.getClientByName(userName);
    if (!user) {
      return policiesFounded;
    }
    policiesFounded = policies.filter(policy => policy.clientId === user.id);
    return policiesFounded;
  }

}
