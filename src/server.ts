import App from "./app";
import ClientRoutes from "./routes/client";
import LoginRoutes from "./routes/login";
import PolicyRoutes from "./routes/policy";

const app = new App(
  [
    new LoginRoutes(),
    new ClientRoutes(),
    new PolicyRoutes()
  ],
);

app.listen();
