import 'dotenv/config';

import * as jwt from 'jsonwebtoken';
import { Response, NextFunction } from 'express';
import IDataStoredInToken from '../models/interfaces/dataStoredInToken';
import IRequestWithUser from '../models/interfaces/requestWithUser';
import UserSchema from '../models/user';
import Unauthorized from  '../exceptions/Unauthorized';

function verifyToken (req: IRequestWithUser, res: Response, next: NextFunction) {
  let token = req.get('token');

  try {
    const verificationResponse = jwt.verify(token, process.env.SEED) as IDataStoredInToken;
    const id = verificationResponse._id;
    UserSchema.findById(id, (err, userDB)=> {
      if (err) {
        next( new Unauthorized('Token') );
      }
      req.user = userDB;
      next();
    });
  }
  catch(err) {
    next( new Unauthorized('Token') );
  }

}

export default verifyToken;
