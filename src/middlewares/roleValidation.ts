import { Response, NextFunction } from 'express';
import IRequestWithUser from '../models/interfaces/requestWithUser';
import CatalogueService from "../services/catalogue";
import Unauthorized from  '../exceptions/Unauthorized';

function verifyAdminRole (req: IRequestWithUser, res: Response, next: NextFunction) {
  let catalogueService = new CatalogueService();

  catalogueService.getClientByEmail(req.user.email).then( (client) => {
    if (client.role === 'admin') {
      next();
    }
    else {
      next( new Unauthorized('Role') );
    }
  });
}

export default verifyAdminRole;
