import { Request, Response, NextFunction } from 'express';
import CatalogueService from "../services/catalogue";
import InternalServerError from  '../exceptions/InternalServerError';
import BadRequest from  '../exceptions/BadRequest';
import NotFound from  '../exceptions/NotFound';

export default class PolicyController {

  public catalogueService: CatalogueService = new CatalogueService();

  public getPolociesByUserName = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let userName = req.query.userName;
      if (!userName) {
        next( new BadRequest('UserName is missing') );
      }
      else {
        let policies = await this.catalogueService.getPolociesByUserName(userName);
        if (!policies) {
          next( new NotFound('No policies found') );
        }
        else {
          return res.status(200).json({
            ok: true,
            policies
          });
        }
      }

    }

    catch (error) {
      next( new InternalServerError() );
    }

  }

}
