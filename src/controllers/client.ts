import { Request, Response, NextFunction } from 'express';
import CatalogueService from "../services/catalogue";
import InternalServerError from  '../exceptions/InternalServerError';
import BadRequest from  '../exceptions/BadRequest';
import NotFound from  '../exceptions/NotFound';

export default class ClientController {

  public catalogueService: CatalogueService = new CatalogueService();

  public getClientByID = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let id = req.params.clientId;
      if (!id) {
        next( new BadRequest('Client ID is missing') );
      }
      else {
        let clientFounded = await this.catalogueService.getClientByID(id);
        if (!clientFounded) {
          next( new NotFound('No Client found') );
        }
        else {
          return res.status(200).json({
            ok: true,
            client: clientFounded
          });
        }
      }
    }

    catch (err) {
      return res.status(500).json({
        ok: true,
        error: err
      });
    }
  }

  public getClientByFilter = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let name = req.query.name;
      let policyName = req.query.policyName;
      if (!name && !policyName) {
        next( new BadRequest('Filter (username or policy name) is missing') );
      }
      else {
        let clientFounded;
        if (name) {
          clientFounded = await this.catalogueService.getClientByName(name);
        }
        else if (policyName) {
          clientFounded = await this.catalogueService.getClientByPolicyId(policyName);
        }
        if (!clientFounded) {
          next( new NotFound('No Client found') );
        }
        else {
          return res.status(200).json({
            ok: true,
            client: clientFounded
          });
        }
      }
    }

    catch (error) {
      next( new InternalServerError() );
    }

  }

}
