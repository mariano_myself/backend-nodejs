import 'dotenv/config';

import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';

import IDataStoredInToken from '../models/interfaces/dataStoredInToken';
import IUser from '../models/interfaces/user';
import UserSchema from '../models/user';

import UserWithThatEmailAlreadyExistsException from  '../exceptions/UserWithThatEmailAlreadyExistsException';
import HttpException from  '../exceptions/HttpException';
import Unauthorized from  '../exceptions/Unauthorized';
import InternalServerError from  '../exceptions/InternalServerError';

export default class LoginController {

  public async login (req: Request, res: Response, next: NextFunction) {
    try {
      let body  = req.body;
      UserSchema.findOne({email: body.email}, (err, userDB:IUser) => {
        if (err) {
          next( new HttpException(400, 'There was a problem with the Login') );
        }

        else if (!userDB) {
          next( new Unauthorized('Email') );
        }
        else if (!bcrypt.compareSync(body.password, userDB.password)) {
          next( new Unauthorized('Password') );
        }
        else {
          const dataStoredInToken: IDataStoredInToken = {
            _id: userDB._id,
          };

          let token = jwt.sign(dataStoredInToken, process.env.SEED, { expiresIn: process.env.CADUCIDAD_TOKEN});

          res.json({
            ok: true,
            user: userDB,
            token
          })
        }
      })
    }

    catch (error) {
      next( new InternalServerError() );
    }

  }

  public async register (req: Request, res: Response, next: NextFunction) {
      try{
        let body  = req.body;
        let userDB: IUser;
        userDB = await UserSchema.findOne( {email: body.email} )
        if ( userDB ) {
          next( new UserWithThatEmailAlreadyExistsException( body.email ) );
        }
        else {
          let user = new UserSchema({
            email: body.email,
            password: bcrypt.hashSync(body.password, 10)
          });

          user.save( (err, userDB) => {
            if ( err ) {
              next( new HttpException(400, 'There was a problem with the Registration') );
            } else {
              res.json({
                ok: true,
                user: userDB
              });
            }
          });

        }
      }

      catch (error){
        next( new InternalServerError() );
      }
  }

}
