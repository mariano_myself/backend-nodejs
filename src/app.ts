import 'dotenv/config';

import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import errorMiddleware from './middlewares/error';

class App {

  public app: express.Application;
  public mongoUrl: string = process.env.URLDB;

  constructor(routes: any[]) {
    this.app = express();
    this.config();
    this.mongoSetup();
    this.initializeRoutes(routes);
    this.initializeErrorHandling();
  }

  public listen() {
    this.app.listen(process.env.PORT, () => {
      console.log(`App listening on the port ${process.env.PORT}`);
    });
  }

  private config(): void{
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private mongoSetup(): void{
    mongoose.connect(this.mongoUrl, {useNewUrlParser: true, bufferCommands:
      false, useCreateIndex: true});
    }

    private initializeRoutes(routes: any[]) {
      routes.forEach((route) => {
        route.routes(this.app);
      });
    }

    private initializeErrorHandling() {
      this.app.use(errorMiddleware);
    }
  }

  export default App;
