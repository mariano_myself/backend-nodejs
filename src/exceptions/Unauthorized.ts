import HttpException from './HttpException';

export default class Unauthorized extends HttpException {
  constructor(item: String) {
    super(401, `${item} is not valid`);
  }
}
