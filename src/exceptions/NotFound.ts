import HttpException from './HttpException';

export default class NotFound extends HttpException {
  constructor(message: string) {
    super(404, message);
  }
}
