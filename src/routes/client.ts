import ClientController from "../controllers/client";
import verifyToken from "../middlewares/authentication";
import verifyAdminRole from "../middlewares/roleValidation";

export default class ClientRoutes {

  public clientController: ClientController = new ClientController();

    public routes(app): void {

        app.route('/client/:clientId')
        .get(verifyToken, this.clientController.getClientByID)

        app.route('/client')
        .get(verifyToken, verifyAdminRole, this.clientController.getClientByFilter)
    }
}
