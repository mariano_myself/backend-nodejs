import PolicyController from "../controllers/policy";
import verifyToken from "../middlewares/authentication";
import verifyAdminRole from "../middlewares/roleValidation";

export default class PolicyRoutes {

  public policyController: PolicyController = new PolicyController();

    public routes(app): void {

        app.route('/policy')
        .get(verifyToken, verifyAdminRole, this.policyController.getPolociesByUserName)
    }
}
