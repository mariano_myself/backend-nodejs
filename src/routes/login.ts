import LoginController from "../controllers/Login";

export default class LoginRoutes {

  public LoginController: LoginController = new LoginController();

  public routes(app): void {

    app.route('/login')
    .post(this.LoginController.login);
    app.route('/register')
    .post(this.LoginController.register);
  }
}
